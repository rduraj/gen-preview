import { askCleanerQuestions, askGeneratorQuestions } from '../prompt'
import { askTaskQuestions } from '../task-questions'

const originalArgv = [ ...process.argv ] // tslint:disable-line

jest.mock('../prompt')

describe('task-questions', () => {

  describe('askTaskQuestion', () => {

    afterEach(() => {
      process.argv = originalArgv // tslint:disable-line
    })

    it ('should run cleaner questions if cleaner mode is defined', () => {
      process.argv.push('--clean-up')

      askTaskQuestions()

      expect(askCleanerQuestions).toHaveBeenCalled()
    })

    it ('should run generator questions if no mode is defined', () => {
      askTaskQuestions()

      expect(askGeneratorQuestions).toHaveBeenCalled()
    })
  })

})
