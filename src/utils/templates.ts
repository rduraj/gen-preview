import pluralize from 'pluralize'
import {
  isFile,
  readFile,
  saveFile,
} from './files'

export type TemplateVariables = {
  readonly name: string,
  readonly [key: string]: string,
}

export type TemplateConfig = {
  readonly extension: 'ts' | 'tsx',
  readonly name: string,
  readonly path: string,
  readonly variables: TemplateVariables,
}

export type TemplateNameList = {
  readonly name: string,
  readonly nameCamelCase: string,
  readonly nameLowerCase: string,
  readonly nameSingular: string,
  readonly nameUpperCase: string,
}

export const saveTemplate = (destPath: string, config: TemplateConfig) => {
  const sourcePath = `${config.path}${config.name}.${config.extension}.tmpl`

  if (!isFile(sourcePath)) {
    throw new Error('Template not found')
  }

  const content = readFile(sourcePath)

  saveFile(
    `${destPath}${config.name}.${config.extension}`,
    parse(config.variables, content),
  )
}

const parse = (variables: TemplateVariables, content: string) => {
  const templateVars = {
    ...getNameVariables(variables.name),
    ...variables,
  }

  return Object.keys(templateVars)
    .reduce(
      (template, key) => template.replace(new RegExp(`\\$\\{${key}\\}`, 'g'), templateVars[key]),
      content,
    )
}

const camelize = (text: string) => text
  .replace(/(?:^\w|[A-Z]|\b\w)/g, (letter, index) => letter.toUpperCase())
  .replace(/[\s|-]+/g, '')

export const getNameVariables = (name: string): TemplateNameList => {
  const nameCamelCase = camelize(name)

  return {
    name,
    nameCamelCase,
    nameLowerCase: nameCamelCase[0].toLowerCase() + nameCamelCase.substr(1),
    nameSingular: pluralize.singular(nameCamelCase),
    nameUpperCase: name.replace('-', '_').toUpperCase(),
  }
}
