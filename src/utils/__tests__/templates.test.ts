import * as files from '../files'
import {
  getNameVariables,
  saveTemplate,
  TemplateConfig,
  TemplateNameList,
} from '../templates'

const sampleTemplate = 'sample template ${nameCamelCase}'
const parsedTemplate = 'sample template PurchaseOrders'
const destPath = 'destination/path/'

jest.mock('../files', () => ({
  isFile: jest.fn(() => true),
  readFile: jest.fn(() => sampleTemplate),
  saveFile: jest.fn(),
}))

const sampleConfig: TemplateConfig = {
  extension: 'ts',
  name: 'PurchaseOrder',
  path: 'sample/path/to/template/',
  variables: {
    name: 'purchase-orders',
  },
}

describe('templates', () => {

  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('saveTemplate', () => {

    it('should throw exception if template file doesn\'t exist', () => {
      jest.spyOn(files, 'isFile').mockImplementation(() => false)

      expect(() => saveTemplate(destPath, sampleConfig)).toThrowError('Template not found')
    })

    it('should get template from path and save parsed content', () => {
      jest.spyOn(files, 'isFile').mockImplementation(() => true)
      const filename = `${sampleConfig.name}.${sampleConfig.extension}`
      const sourcePath = `${sampleConfig.path}${filename}`

      saveTemplate(destPath, sampleConfig)

      expect(files.readFile).toHaveBeenCalledWith(sourcePath)
      expect(files.saveFile).toHaveBeenCalledWith(`${destPath}${filename}`, parsedTemplate)
    })

  })

  describe('getNameVariables', () => {
    it('should prepare list of names based on given string', () => {
      const givenName = 'purchase-orders'
      const nameList: TemplateNameList = {
        name: givenName,
        nameCamelCase: 'PurchaseOrders',
        nameLowerCase: 'purchaseOrders',
        nameSingular: 'PurchaseOrder',
        nameUpperCase: 'PURCHASE_ORDERS',
      }

      expect(getNameVariables(givenName)).toEqual(nameList)
    })
  })

})
