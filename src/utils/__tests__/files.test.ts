import * as fs from 'fs'
import mockFs from 'mock-fs'
import * as files from '../files'

const rmdirR = files.rmdirR
const cleanFolder = files.cleanFolder

jest.mock('fs', () => {
  const originalFs = require.requireActual('fs')

  return {
    ...originalFs,
    rmdirSync: jest.fn(),
    unlinkSync: jest.fn(),
  }
})

describe('files', () => {

  beforeEach(() => {
    mockFs({
      'nested/structure/of/folders': {
        'with-one-file.txt': '',
      },
      'to-clean': {
        store: {},
        users: {},
      },
    })
  })

  afterEach(() => {
    mockFs.restore()
  })

  describe('rmdirR', () => {

    beforeEach(() => {
      jest.clearAllMocks()
    })

    it('should check if directory exists and stop if not', () => {
      expect(rmdirR('nested/structure/folders')).toEqual(false)
      expect(rmdirR('nested/structure/of/folders')).toEqual(true)
    })

    it('should run unlinkSync for each file in given path', () => {
      jest.spyOn(fs, 'unlinkSync')

      rmdirR('nested/structure')

      expect(fs.unlinkSync).toHaveBeenCalledTimes(1)
    })

    it('should run rmdirSync for each directory in given path', () => {
      const numberOfDirectories = 4

      jest.spyOn(fs, 'rmdirSync')
      rmdirR('nested/')

      expect(fs.rmdirSync).toHaveBeenCalledTimes(numberOfDirectories)
    })

  })

  describe('cleanFolders', () => {

    beforeEach(() => {
      jest.spyOn(files, 'rmdirR').mockImplementation(() => true)

      jest.clearAllMocks()
    })

    it('should remove folders and files from given path', () => {
      const dirCount = 2

      cleanFolder('to-clean/')

      expect(files.rmdirR).toHaveBeenCalledTimes(dirCount)
    })

    it('should omit given directories from given path', () => {

      const dirCount = 2
      cleanFolder('to-clean/', [ 'store' ])

      expect(files.rmdirR).toHaveBeenCalledTimes(dirCount - 1)
    })
  })

})
