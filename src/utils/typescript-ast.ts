import ts from 'typescript'
import { isFile, readFile, saveFile } from './files'
import { lint } from './linter'

export type Transformer = (file: ts.Node, name?: string) => ts.NodeArray<ts.Node>

const readTsFile = (path: string) => ts.createSourceFile(
  path,
  readFile(path).toString(),
  ts.ScriptTarget.ES2015,
  true,
)

const updateTsFile = (file: ts.SourceFile, transformer: ts.NodeArray<ts.Node>) => {
  const printer = ts.createPrinter({
    newLine: ts.NewLineKind.LineFeed,
    omitTrailingSemicolon: true,
  })

  const result = printer.printList(
    ts.ListFormat.MultiLine,
    transformer,
    file,
  ).toString()

  return result
    .replace(/</g, '\n<')
    .replace(/>/g, '>\n')
}

export const transformFile = (filePath: string, transformer: Transformer, name?: string) => {
  if (isFile(filePath)) {
    const file = readTsFile(filePath)
    const updated = updateTsFile(file, transformer(file, name))

    saveFile(filePath, updated)
    lint(filePath, updated)
  }
}
