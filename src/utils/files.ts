import * as fs from 'fs'

export const rmdirR = (path: string) => {
  if (!fs.existsSync(path)) {
    return false
  }

  if (!isDirectory(path)) {
    return fs.unlinkSync(path)
  }

  fs
    .readdirSync(path)
    .forEach((file: string) => {
      const currentPath = `${path}/${file}`

      if (isDirectory(currentPath)) {

        return rmdirR(currentPath)
      }

      return fs.unlinkSync(currentPath)
    })

  fs.rmdirSync(path)

  return true
}

export const readFile = (path: string) => fs.readFileSync(path, 'utf8')

export const saveFile = (path: string, content: string) => {
  fs.writeFileSync(path, content, 'utf8')
}

export const createDir = (path: string) => fs.mkdirSync(path)

export const isFile = (path: string) => fs.statSync(path).isFile()

export const isDirectory = (path: string) => fs.lstatSync(path).isDirectory()

export const cleanFolder = (path: string, toOmit: ReadonlyArray<string> = []) => {
  fs.readdirSync(path)
    .filter(directory => toOmit.indexOf(directory) === -1)
    .forEach(directory => { rmdirR(`${path}${directory}`) })
}
