export const projectLibName = process.env.PROJECT_LIB_NAME
export const projectPath = process.env.PROJECT_PATH || '.'
export const libPath = `${projectPath}/src/${projectLibName}`
export const appPath = `${projectPath}/src/App/`
export const generatorPath = '../'

export const epicsPath = `${libPath}store/epic.ts`
export const statePath = `${libPath}store/state.ts`
export const reducersPath = `${libPath}store/reducer.ts`
