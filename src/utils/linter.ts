import { Linter } from 'tslint'
import { projectPath } from './path'

const linter = new Linter({ fix: true })

export const lint = (path: string, content: string) => linter.lint(
  path,
  content,
  Linter.loadConfigurationFromPath(`${projectPath}/tslint.json`),
)
