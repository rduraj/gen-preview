import { dataLayersToSkip } from '../config'
import { cleanEpics, cleanReducers, cleanStore } from '../transformers'
import { cleanFolder, libPath } from '../utils'

export const cleanDataLayer = () => {

  cleanFolder(libPath, dataLayersToSkip)

  cleanEpics()
  cleanReducers()
  cleanStore()
}
