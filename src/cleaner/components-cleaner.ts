import { componentsToSkip } from '../config/cleaner'
import {
  appPath,
  cleanFolder,
  saveTemplate,
  TemplateConfig,
} from '../utils'

const updateRouter = () => {
  const templateConfig: TemplateConfig = {
    extension: 'tsx',
    name: 'Router',
    path: `${__dirname}/../templates/components/`,
    variables: { name: 'router' },
  }

  saveTemplate(appPath, templateConfig)
}

export const cleanComponents = () => {
  cleanFolder(appPath, componentsToSkip)
  updateRouter()
}
