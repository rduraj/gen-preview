import { AnswerTypes, CleanerAnswers } from '../prompt'
import { cleanComponents } from './components-cleaner'
import { cleanDataLayer } from './data-layer-cleaner'

export const runCleaner = (answers: CleanerAnswers) => {
  if (answers[AnswerTypes.CleanConfirm] === 'Y') {
    cleanComponents()
    cleanDataLayer()
  }
}
