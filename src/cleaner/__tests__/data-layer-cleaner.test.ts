import { dataLayersToSkip } from '../../config/cleaner'
import {
  cleanFolder,
  libPath,
} from '../../utils'
import { cleanDataLayer } from '../data-layer-cleaner'

const directoriesList: ReadonlyArray<string> = ['Users', 'Orders']

jest.mock('fs', () => ({
  readdirSync: jest.fn(() => [...directoriesList]),
}))

jest.mock('../../transformers', () => ({
  cleanEpics: jest.fn(),
  cleanReducers: jest.fn(),
  cleanStore: jest.fn(),
}))

jest.mock('../../utils', () => ({
  cleanFolder: jest.fn(),
  isFile: jest.fn(() => false),
  rmdirR: jest.fn(path => path),
  saveTemplate: jest.fn(),
}))

describe('data-layer-cleaner', () => {
  describe('cleanDataLayer', () => {
    afterEach(() => {
      jest.resetAllMocks()
    })

    it ('should remove folders from -lib and omit configured', () => {
      cleanDataLayer()

      expect(cleanFolder).toHaveBeenCalledTimes(1)
      expect(cleanFolder).toHaveBeenCalledWith(libPath, dataLayersToSkip)
    })
  })
})
