import * as fs from 'fs'
import { componentsToSkip } from '../../config/cleaner'
import {
  appPath,
  cleanFolder,
  saveTemplate,
} from '../../utils'
import { cleanComponents } from '../components-cleaner'

const directoriesList: ReadonlyArray<string> = ['Users', 'Orders']

jest.mock('fs', () => ({
  readdirSync: jest.fn(() => [...directoriesList]),
}))

jest.mock('../../utils', () => ({
  cleanFolder: jest.fn(),
  isFile: jest.fn(() => false),
  rmdirR: jest.fn(path => path),
  saveTemplate: jest.fn(),
}))

describe('components-cleaner', () => {
  describe('cleanComponents', () => {
    afterEach(() => {
      jest.resetAllMocks()
    })

    it ('should remove folders from `App` and omit configured', () => {
      cleanComponents()

      expect(cleanFolder).toHaveBeenCalledTimes(1)
      expect(cleanFolder).toHaveBeenCalledWith(appPath, componentsToSkip)
    })

    it ('should restore router to default template', () => {
      jest.spyOn(fs, 'readdirSync').mockImplementation(() => ([...directoriesList]))
      cleanComponents()

      expect(saveTemplate).toHaveBeenCalledTimes(1)
    })

  })
})
