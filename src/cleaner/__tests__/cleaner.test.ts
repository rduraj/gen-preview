import { CleanerAnswers } from '../../prompt'
import { runCleaner } from '../cleaner'
import { cleanComponents } from '../components-cleaner'
import { cleanDataLayer } from '../data-layer-cleaner'

jest.mock('../components-cleaner', () => ({
  cleanComponents: jest.fn(),
}))

jest.mock('../data-layer-cleaner', () => ({
  cleanDataLayer: jest.fn(),
}))

describe('cleaner', () => {

  it('should run cleaning if answer is Y', () => {
    const confirm: CleanerAnswers = {
      CleanConfirm: 'Y',
    }

    runCleaner(confirm)

    expect(cleanComponents).toHaveBeenCalled()
    expect(cleanDataLayer).toHaveBeenCalled()
  })

})
