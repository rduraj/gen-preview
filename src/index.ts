import { runCleaner } from './cleaner'
import { runGenerator } from './generator'
import { CleanerAnswers, GeneratorAnswers } from './prompt'
import { askTaskQuestions, isCleanerMode } from './task-questions'

askTaskQuestions()
  .then(answers => {
    if (isCleanerMode()) {
      runCleaner(answers as CleanerAnswers)
    } else {
      runGenerator(answers as GeneratorAnswers)
    }
  })
