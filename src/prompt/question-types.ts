export enum AnswerTypes {
  CleanConfirm = 'CleanConfirm',
  DataLayerElements = 'DataLayerElements',
  Name = 'Name',
  Type = 'Type',
}

export enum QuestionTypes {
  DataLayer = 'DataLayer',
}

export enum QuestionInput {
  Checkbox = 'checkbox',
  Radio = 'radio',
  Text = 'input',
}
