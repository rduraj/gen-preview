import Enquirer from 'enquirer'
import { default as PromptCheckbox } from 'prompt-checkbox'
import { default as PromptRadio } from 'prompt-radio'

const enquirer = new Enquirer()

import { DataLayerElements } from '../config'
import { AnswerTypes, QuestionInput, QuestionTypes } from './question-types'

export type CleanerAnswers = {
  readonly [AnswerTypes.CleanConfirm]: string,
}

export type GeneratorAnswers = {
  readonly [AnswerTypes.DataLayerElements]: string,
  readonly [AnswerTypes.Name]: string,
  readonly [AnswerTypes.Type]: string,
}

enquirer.register(QuestionInput.Checkbox, PromptCheckbox)
enquirer.register(QuestionInput.Radio, PromptRadio)

enquirer.question(AnswerTypes.Type, {
  choices: Object.keys(QuestionTypes),
  default: QuestionTypes.DataLayer,
  message: 'What do you want to generate?',
  name: AnswerTypes.Type,
  type: QuestionInput.Radio,
})

enquirer.question(AnswerTypes.Name, {
  message: 'Provide name of your new element (use lowercases and dashes, eg. purchase-orders)',
  name: AnswerTypes.Name,
  type: QuestionInput.Text,
  validate: (name: string) => {
    const matched = name.match(/([a-z]|-)+/g)

    return !!name && matched && matched.length === 1
  },
})

enquirer.question(AnswerTypes.DataLayerElements, {
  choices: Object.keys(DataLayerElements),
  message: 'Pick your data-layer elements:',
  name: AnswerTypes.DataLayerElements,
  type: QuestionInput.Checkbox,
})

enquirer.question(AnswerTypes.CleanConfirm, {
  message: 'Are you sure you want to clean the project? [Y/n]',
  name: AnswerTypes.CleanConfirm,
  type: QuestionInput.Text,
})

export const askCleanerQuestions = (): Promise<GeneratorAnswers> => enquirer
  .prompt([AnswerTypes.CleanConfirm])

export const askGeneratorQuestions = (): Promise<CleanerAnswers> => enquirer
  .prompt([AnswerTypes.Type, AnswerTypes.Name])
  .then((answers: GeneratorAnswers) => {
    if (answers[AnswerTypes.Type] === QuestionTypes.DataLayer) {
      return enquirer.ask(DataLayerElements)
    }
  })
