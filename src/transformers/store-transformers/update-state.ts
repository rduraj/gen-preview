import ts from 'typescript'
import { getNameVariables } from '../../utils'

export const updateStateTransformer = (file: ts.SourceFile, name: string) => {
  const names = getNameVariables(name)
  const importElements = `{ ${names.name}Map }`
  const importPath = `../${names.name}`

  return ts.createNodeArray([
    ts.createImportDeclaration(
      /* decorators */ undefined,
      /* modifiers */ undefined,
      ts.createImportClause(ts.createIdentifier(importElements), undefined),
      ts.createLiteral(importPath),
    ),
    ...file.statements.map(node => {

      if (!ts.isInterfaceDeclaration(node)) {
        return node
      }

      // tslint:disable
      const member: any = {
        ...node.members[0],
        name: ts.createIdentifier(names.nameLowerCase),
        type: ts.createIdentifier(`${names.name}Map`),
      }
      // tslint:enable

      return ts.updateInterfaceDeclaration(
        node,
        node.decorators,
        node.modifiers,
        node.name,
        node.typeParameters,
        node.heritageClauses,
        [
          member,
          ...node.members,
        ],
      )
    }),
  ])
}
