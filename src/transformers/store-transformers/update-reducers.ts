import ts from 'typescript'
import { getNameVariables } from '../../utils'

export const updateReducersTransformer = (file: ts.SourceFile, name: string) => {
  const names = getNameVariables(name)
  const importElements = `{ ${names.nameLowerCase}Reducer }`
  const importPath = `../${names.name}`

  return ts.createNodeArray([
    ts.createImportDeclaration(
      /* decorators */ undefined,
      /* modifiers */ undefined,
      ts.createImportClause(ts.createIdentifier(importElements), undefined),
      ts.createLiteral(importPath),
    ),
    ...file.statements.map(node => {

      if (!ts.isVariableStatement(node)) {
        return node
      }

      const reducerDeclaration = node.declarationList.declarations[0]

      if (!reducerDeclaration.initializer) {
        return node
      }

      // tslint:disable
      const reducerArguments = [ ...(reducerDeclaration.initializer as any).arguments ]
      reducerArguments[0]
        .properties
        .push(ts.createIdentifier(`${names.nameLowerCase}: ${names.nameLowerCase}Reducer`))

      const reducerInitializer = {
        ...reducerDeclaration.initializer,
        reducerArguments,
      }
      // tslint:enable

      return ts.createVariableStatement(
        node.modifiers,
        ts.createVariableDeclarationList(
          [
            ts.createVariableDeclaration(
              reducerDeclaration.name,
              reducerDeclaration.type,
              reducerInitializer,
            ),
          ],
        ),
      )
    }),
  ])
}
