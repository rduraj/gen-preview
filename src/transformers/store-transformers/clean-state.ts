
import ts from 'typescript'
import { stateDeclarationsToStay, stateImportsToStay } from '../../config'

export const cleanStateTransformer = (file: ts.SourceFile) =>
  ts.createNodeArray(
    file
      .statements
      .filter(node =>
        !(ts.isImportDeclaration(node) && stateImportsToStay.indexOf(node.moduleSpecifier.getText().replace(/'/g, '')) === -1),
      )
      .map(node => {
        if (!ts.isTypeAliasDeclaration(node)) {
          return node
        }

        const nodeType = {
            ...node.type,
            members: {
              ...(node.type as ts.TypeLiteralNode)
                .members
                .filter((member: ts.TypeElement) =>
                  stateDeclarationsToStay.indexOf(member.name ? member.name.getText().replace(/'/g, '') : '') > -1
                ),
            },
          }

        return ts.updateTypeAliasDeclaration(
          node,
          node.decorators,
          node.modifiers,
          node.name,
          node.typeParameters,
          nodeType,
        )
      }),
  )
