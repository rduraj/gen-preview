import ts from 'typescript'
import { getNameVariables } from '../../utils'

export const updateEpicsTransformer = (file: ts.SourceFile, name: string) => {
  const names = getNameVariables(name)
  const importElements = `{ ${names.nameLowerCase}Epic, ${names.name}Action }`
  const importPath = `../${names.name}`

  return ts.createNodeArray([
    ts.createImportDeclaration(
      /* decorators */ undefined,
      /* modifiers */ undefined,
      ts.createImportClause(ts.createIdentifier(importElements), undefined),
      ts.createLiteral(importPath),
    ),
    ...file.statements.map(node => {

      if (!ts.isVariableStatement(node)) {
        return node
      }

      if (node.declarationList.declarations[0].name.getText() !== 'rootEpic') {
        return node
      }

      const rootEpicDeclaration = node.declarationList.declarations[0]

      if (!rootEpicDeclaration.initializer) {
        return node
      }

      // tslint:disable
      const typeArguments = [...(rootEpicDeclaration.initializer as any).typeArguments]
      typeArguments[typeArguments.length - 1].types.push(ts.createIdentifier(`${names.name}Action`))

      const rootEpicInitializer = {
        ...rootEpicDeclaration.initializer,
        arguments: [
          ...(rootEpicDeclaration.initializer as any).arguments,
          ts.createIdentifier(`${names.nameLowerCase}Epic`)
        ],
        typeArguments,
      }
      // tslint:enable

      return ts.createVariableStatement(
        node.modifiers,
        ts.createVariableDeclarationList(
          [
            ts.createVariableDeclaration(
              rootEpicDeclaration.name,
              rootEpicDeclaration.type,
              rootEpicInitializer,
            ),
          ],
        ),
      )
    }),
  ])
}
