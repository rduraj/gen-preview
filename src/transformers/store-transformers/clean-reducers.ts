import ts from 'typescript'
import { reducerDeclarationsToStay, reducersToStay } from '../../config'

export const cleanReducerTransformer = (file: ts.SourceFile) =>
  ts.createNodeArray(
    file
      .statements
      .filter(node => !(ts.isImportDeclaration(node) && reducersToStay.indexOf(node.moduleSpecifier.getText().replace(/'/g, '')) === -1))
      .map(node => {
        if (!ts.isVariableStatement(node)) {
          return node
        }

        const reducerDeclaration = node.declarationList.declarations[0]

        if (!reducerDeclaration.initializer) {
          return node
        }

        // tslint:disable
        const properties = (reducerDeclaration.initializer as any).arguments[0]
          ? (reducerDeclaration.initializer as any).arguments[0].properties || []
          : []

        const rootEpicInitializer = {
          ...reducerDeclaration.initializer,
          arguments: [
            {
              ...(reducerDeclaration.initializer as any).properties,
              properties: [
                ...properties.filter((property: any) => reducerDeclarationsToStay.indexOf(property.name.escapedText) > -1)
              ],
            }
          ],
        }
        // tslint:enable

        return ts.createVariableStatement(
          node.modifiers,
          ts.createVariableDeclarationList(
            [
              ts.createVariableDeclaration(
                reducerDeclaration.name,
                reducerDeclaration.type,
                rootEpicInitializer,
              ),
            ],
            ts.NodeFlags.Const,
          ),
        )
      }),
  )
