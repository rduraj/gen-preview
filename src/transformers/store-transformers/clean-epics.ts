import ts from 'typescript'
import { epicsToStay } from '../../config/cleaner'
import { Transformer } from '../../utils'

export const cleanEpicsTransformer: Transformer = (file: ts.SourceFile) => ts.createNodeArray(
  file
    .statements
    .filter((node: ts.Node) =>
      !(ts.isImportDeclaration(node) && epicsToStay.indexOf(node.moduleSpecifier.getText().replace(/'/g, '')) === -1),
    )
    .map((node: ts.Node) => {
      if (!ts.isVariableStatement(node)) {
        return node
      }

      if (node.declarationList.declarations[0].name.getText() !== 'rootEpic') {
        return node
      }

      const rootEpicDeclaration = node.declarationList.declarations[0]

      if (!rootEpicDeclaration.initializer) {
        return node
      }

      // tslint:disable
      const typeArguments = rootEpicDeclaration.initializer && (rootEpicDeclaration.initializer as any).typeArguments || []
      const rootEpicInitializer = {
        ...rootEpicDeclaration.initializer,
        arguments: [],
        typeArguments: typeArguments.filter((argument: any) => !argument.types)
      }
      // tslint:enable

      return ts.createVariableStatement(
        node.modifiers,
        ts.createVariableDeclarationList(
          [
            ts.createVariableDeclaration(
              rootEpicDeclaration.name,
              rootEpicDeclaration.type,
              rootEpicInitializer,
            ),
          ],
          ts.NodeFlags.Const,
        ),
      )
    }),
)
