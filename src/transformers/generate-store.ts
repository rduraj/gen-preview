import {
  epicsPath,
  reducersPath,
  statePath,
  transformFile,
} from '../utils'
import {
  updateEpicsTransformer,
  updateReducersTransformer,
  updateStateTransformer,
} from './store-transformers'

export const updateEpics = (name: string) => {
  transformFile(epicsPath, updateEpicsTransformer, name)
}

export const updateReducers = (name: string) => {
  transformFile(reducersPath, updateReducersTransformer, name)
}

export const updateStore = (name: string) => {
  transformFile(statePath, updateStateTransformer, name)
}
