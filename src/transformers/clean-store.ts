import {
  epicsPath,
  reducersPath,
  statePath,
  transformFile,
} from '../utils'
import {
  cleanEpicsTransformer,
  cleanReducerTransformer,
  cleanStateTransformer,
} from './store-transformers'

export const cleanEpics = () => {
  transformFile(epicsPath, cleanEpicsTransformer)
}

export const cleanReducers = () => {
  transformFile(reducersPath, cleanReducerTransformer)
}

export const cleanStore = () => {
  transformFile(statePath, cleanStateTransformer)
}
