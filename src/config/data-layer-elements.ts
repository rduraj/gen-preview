export enum DataLayerElements {
  Actions = 'Actions',
  Epic= 'Epic',
  Reducer= 'Reducers',
  Selectors= 'Selectors',
}
