export const componentsToSkip: ReadonlyArray<string> = [
  'Router',
]

export const dataLayersToSkip: ReadonlyArray<string> = [
  'store',
  'router',
]

export const epicsToStay: ReadonlyArray<string> = ['redux', './state']

export const reducersToStay: ReadonlyArray<string> = ['redux', '../router', './state']

export const reducerDeclarationsToStay: ReadonlyArray<string> = ['router']

export const stateImportsToStay: ReadonlyArray<string> = ['react-router-redux']

export const stateDeclarationsToStay: ReadonlyArray<string> = ['router']
