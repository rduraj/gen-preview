import { askCleanerQuestions, askGeneratorQuestions, CleanerAnswers, GeneratorAnswers } from './prompt'

export const isCleanerMode = (): boolean => process.argv.indexOf('--clean-up') > -1

export const askTaskQuestions = (): Promise<CleanerAnswers | GeneratorAnswers> => isCleanerMode()
  ? askCleanerQuestions()
  : askGeneratorQuestions()
